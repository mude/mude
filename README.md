# Python Tools for Modelling, Uncertainty, and Data Analysis for Engineers
This package contains a set of tools that are used to support the MUDE course given at Civil Engineering, TU Delft, for first-year MSc students.
It contains a set of tools and dependencies that are used in the course contents themselves.
In addition, this package has several debugging tools that can be used by students and staff to check that their installation is compatible with the requirements for the course.

## Authors
This package was developed by the MUDE teaching team at the TU Delft Faculty of Civil Engineering and Geosciences.

## License
Per the default suggested in the roadmap “Copyright and open licenses in online education” of the TU Delft Open Education Consortium, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.